;;; ~/.config/doom/changes-mode.el --- Mode for RPM changes files -*- lexical-binding: t; -*-
;;; Commentary:
;;; Major mode that sets the fill-column for rpm .changes files
;;; Code:

;;;###autoload
(define-derived-mode changes-mode text-mode "Changes"
  "Major mode for editing rpm .changes files."
  (setq-local fill-column 67))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.changes\\'" . changes-mode))

(provide 'changes-mode)
;;; changes-mode.el ends here
