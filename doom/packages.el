;;; ~/.config/doom/packages.el -*- no-byte-compile: t; -*-
;;; Major modes
(package! salt-mode :pin "c46b24e7fdf4a46df5507dc9c533bbc0064a46fa")
(package! systemd-mode :recipe (:files (:defaults "*.txt"))
  :pin "8742607120fbc440821acbc351fda1e8e68a8806")
(package! feature-mode :pin "afd49b8a8504e5874027fc0a46283adb1fea26c0")

;;; Minor modes
(package! sphinx-doc :pin "1eda612a44ef027e5229895daa77db99a21b8801")
(package! org-appear  :pin "32ee50f8fdfa449bbc235617549c1bccb503cb09")
;; (package! acme-mouse :recipe (:host github :repo  "akrito/acme-mouse"))
(when (modulep! :tools lsp +eglot)
  (package! lsp-docker :disable t :pin "ce291d0f80533f8eaca120eb745d55669e062636")) ; pulls in lsp-mode
(package! page-break-lines :recipe (:host github :repo "purcell/page-break-lines")
  :pin "247854f6a82b5c7ab5ae493eb5390c5079cd032d")
(package! ligature :pin "6ac1634612dbd42f7eb81ecaf022bd239aabb954")
(package! ultra-scroll :recipe (:host github :repo "jdtsmith/ultra-scroll") :pin "2e3b9997ae1a469e878feaa0af23a23685a0fbed")
(package! smartscan :pin "234e077145710a174c20742de792b97ed2f965f6")
(package! whole-line-or-region :pin "052676394c675303d6b953209e5d2ef090fbc45d")
(package! colorful-mode :pin "5c7831073f687738a8ffae2b78f9bea48c8a140b")

;;; Tools
;; Take screenshots of buffers easily
(package! screenshot :recipe (:host github :repo "tecosaur/screenshot" :build (:not compile))
  :pin "2770c0cfefe1cc09d55585f4f2f336a1b26e610e")
(package! uyuni-chlog :recipe (:host github :repo "agraul/uyuni-chlog") :pin "169252489a1e3f28c46f6b00a48583312b265731")
(package! osc :recipe (:local-repo "~/src/osc-emacs/"))
(package! pg :recipe (:host github :repo "emarsden/pg-el") :pin "e6199fa1e883c0e6eb47ce6547bd2a782e35820e")
(package! pgmacs :recipe (:host github :repo "emarsden/pgmacs") :pin "f57d7a2daa34e04f060e27972dd641a4ef804c0d")

;;; Themes
(package! modus-themes :pin "75791f5efc49ebb7e714fe2e0d813a0d1e70c25d")
(package! doom-alabaster-theme :recipe (:local-repo "~/src/doom-alabaster-theme"))
(package! acme-theme :recipe (:local-repo "~/src/emacs-acme-theme"))
(package! ef-themes :pin "85d3065caca862596e013e2a4a55e2f8bd81e43e")

;;; Misc
(package! german-holidays :recipe (:host github :repo "rudolfochrist/german-holidays")
  :pin "a8462dffccaf2b665f2032e646b5370e993a386a")
