* agraul's dotfiles

This is a repository of my /frequently changed/ configuration files. Why only
those and not all? Fair question, I use =salt= to manage my initial setup of my
systems. Not every system requires =git= which is needed to clone this
repository. Files I rarely change live in my =salt-states= repository and are
copied from there into my system. Configuration files for applications I often
change are instead tracked here. This helps me with changing them without going
through the =salt= deploy circle. Is it a smart idea to split them? I don't
know yet, I will see.

This repository includes the the configuration for:
- Doom Emacs
- Tridactyl
- Tmux
