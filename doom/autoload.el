;;; ~/.config/doom/autoload.el -*- lexical-binding: t; -*-

;;;###autoload
(defun agraul/symbol-rename ()
  (interactive)
  (call-interactively
    (cond ((bound-and-true-p lsp-managed-mode) #'lsp-rename)
          ((bound-and-true-p eglot-mode) #'eglot-rename)
          ((bound-and-true-p smartscan-mode) #'smartscan-symbol-replace)
          (t (lambda () (interactive) (message "No rename provider available."))))))

;;;###autoload
(defun agraul/toggle-theme ()
  "Toggle between dark and light theme."
  (interactive)
  (setq doom-theme (if (eq doom-theme agraul/dark-theme)
                       agraul/light-theme
                     agraul/dark-theme))
  (load-theme doom-theme t))

;; See discussion at
;; https://old.reddit.com/r/emacs/comments/cgbpvl/opening_media_files_straight_from_gnu_emacs_dired/
;;;###autoload
(defun agraul/xdg-open (file)
  "Open FILE with xdg-open."
  (interactive "f")
  (let ((process-connection-type nil))
    (start-process "" nil shell-file-name
                   shell-command-switch
                   (format "nohup &>/dev/null xdg-open %s"
                           (shell-quote-argument (expand-file-name file))))))

;;;###autoload
(defun agraul/xdg-open-at-point ()
  "Open file at point with xdg-open."
  (interactive)
  (agraul/xdg-open (thing-at-point 'filename)))

;;;###autoload
(defun agraul/ssh-root-shell (&optional host)
  "Open an `shell' over SSH."
  (interactive "sHost: ")
  (let ((default-directory (format "/ssh:root@%s:/" host)))
    (funcall #'shell)))
